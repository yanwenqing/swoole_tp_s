<?php

namespace app\index\controller;

use Swoole\Coroutine\Redis;
use think\Request;

class Index
{
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index()
    {
        $clients = \think\facade\Cache::get('clients');
        var_dump($clients);

        return 'index index';
    }

    public function chart(){
        //var_dump($_POST['http_server']);
        foreach ($_POST['http_server']->ports[1]->connections as $fd){
            $_POST['http_server']->push($fd, $fd);
        }

        return '123123';
    }

    public function post_data(){
        // var_dump($_GET);
        $terms = [
            1 => [
                'name' => '马刺',
                'logo' => '/live/imgs/team1.png'
            ],
            4 => [
                'name' => '火箭',
                'logo' => '/live/imgs/team2.png'
            ]
        ];

        $data = [
            'type' => $_GET['type'],
            'title' => '直播员',
            'content' => $_GET['content']
        ];
        $taskData = [
            'method' => 'push',
            'data' => $data
        ];
        $_POST['http_server']->task($taskData, 0);

        return 'post data';
    }

    public function image(){
        // var_dump(\request()->file('file'));
        $file = \request()->file('file');
        $res = $file->move('public/static/upload');
        // var_dump($res);

        return 'upload/'.$res->getSaveName();
    }

    public function hello($name = 'ThinkPHP5')
    {
        return 'hello,' . $name;
    }

    /**
     * 发送验证码
     * @return string
     */
    public function sendCode()
    {
        $code = rand(1000, 9999);
        $phone = $this->request->param('phone');
        $taskData = [
            'method' => 'send',
            'data' => [
                'code' => $code,
                'phone' => $phone
            ]
        ];
        $_POST['http_server']->task($taskData);

        return $code;
    }
}
