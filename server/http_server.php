<?php
/**
 * Created by PhpStorm.
 * User: baidu
 * Date: 18/2/28
 * Time: 上午1:39
 */
$http = new swoole_http_server("0.0.0.0", 8812);

$http->set([
    'enable_static_handler' => true,
    //'document_root' => __DIR__."/../public/static/live",
    'document_root' => "/mnt/hgfs/D/www/swoole_tp_s/public/static/live",
    'work_num' => 5
]);

$http->on('WorkerStart', function(swoole_server $server, $worker_id){
    // 加载基础文件
    require __DIR__ . '/../thinkphp/base.php';
});

$http->on('request', function ($request, $response) use ($http){
    // 设置全局变量
    // $_SERVER = [];
    if(isset($request->server)){
        foreach ($request->server as $k => $v){
            $_SERVER[strtoupper($k)] = $v;
        }
    }
    if(isset($request->header)){
        foreach ($request->header as $k => $v){
            $_SERVER[strtoupper($k)] = $v;
        }
    }

    $_GET = [];
    if(isset($request->get)){
        foreach ($request->get as $k => $v){
            $_GET[strtoupper($k)] = $v;
        }
    }
    $_POST = [];
    if(isset($request->post)){
        foreach ($request->post as $k => $v){
            $_POST[strtoupper($k)] = $v;
        }
    }
    // print_r($request->server);

    ob_start();
    // 执行应用并响应
    try{
        \think\Container::get('app')->run()->send();
    }catch (Exception $exception){
        var_dump($exception);
    }
    //echo PHP_EOL."action = ".(request()->action()).PHP_EOL;

    $res = ob_get_contents();
    ob_end_clean();

    // 输出缓冲区内容
    $response->end($res);

    // $http->close();
});

$http->start();